CONTENTS OF THIS FOLDER:

Report in pdf and font files
Presentation in PPT and pdf.
Folder containing the original dataset used
Subfolder with intermediate data files used during the development of the work
Folder containing bibliographic references available in digital support plus the reference files when possible
Font code of the R scripts used and macros from other softwares or programming languages used
